# from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.urls import reverse_lazy

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"
    context_object_name = "todos"
    paginate_by = 8


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"
    context_object_name = "todo"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "todos/create.html"
    # context_object_name = "todo"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_detail", kwargs={"pk": self.object.pk})


class TodoListUpdateView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    # context_object_name = "todo"
    fields = ["name"]

    def get_success_url(self):
        return reverse_lazy("todo_detail", kwargs={"pk": self.object.pk})


class TodoListDeleteView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    context_object_name = "todo"
    success_url = reverse_lazy("todo_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "todos/newitem.html"
    # context_object_name = "todo"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_detail", kwargs={"pk": self.object.list.pk})


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "todos/edititem.html"
    # context_object_name = "todo"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_detail", kwargs={"pk": self.object.list.pk})